import React from 'react';
import NavBar from './Navbar';
import Background from './background.jpg';
import './home.css';
import {Button} from 'react-bootstrap';
import About from './About';

const Greetings = () => {
    var welcome;
    var date = new Date();
    var hour = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();
    if (minute < 10) {
        minute = "0" + minute;
    }
    if (second < 10) {
        second = "0" + second;
    }
    if (hour < 12) {
        welcome = "Good Morning!";
    } else if (hour < 17) {
        welcome = "Good Afternoon!";
    } else {
        welcome = "Good Evening!";
    }
    return (
        <div className='greeting blinker'>
            <h1>
                <font>{welcome}</font>
            </h1>
            <h1 className='mainHeading'> I am Neetha Francis :)</h1>
        </div>
    );
}
class Home extends React.Component {

    render() {
        return (<React.Fragment>
            <div className='header'>
                <NavBar />
            </div>

            <div className='jumbotron'>
            <div className='container'>
                <Greetings />
            </div>
            <br/>
            <div className='button-container'>
             <Button variant="secondary" size="lg" href="#about">
                Know more about me
                </Button>
                </div>
            </div>

            <div className='about'>
            
            <About />

            </div>

        </React.Fragment>
        );
    }

}
export default Home;
