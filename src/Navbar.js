import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';
class NavBar extends React.Component {
    render() {
        return (
            <>
                    <Navbar style={{ position: "fixed", top: 0 }} bg="dark" variant="dark" fixed="top">
                        <Navbar.Brand href="#home">Neetha Francis</Navbar.Brand>
                        <Nav className="mr-auto">
                            <Nav.Link href="#home">Home</Nav.Link>
                            <Nav.Link href="#about">About Me</Nav.Link>
                            <Nav.Link href="#skills">Skills</Nav.Link>
                            <Nav.Link href="#contact">Contact Me</Nav.Link>
                        </Nav>
                    </Navbar>
                
            </>
        )
    }
}

export default NavBar;