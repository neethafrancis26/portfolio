import React, { Component } from 'react'
import './About.css';

export default class About extends Component {
  render() {
    return (<div><div className="container-fluid home-content1">
		<div className="row" id="about">
			<div className="col-md-6 content1-left">
				<h3>About Me <span className="blinker">!</span></h3>
				<p>I am an aspiring Web Developer and B.Tech Student form Kerala, India. I like to build new things</p>
			</div>
			<div className="col-md-6 content1-right">
				<h1 style={{textAlign:'left'}}>Education</h1>
                <p>Federal Institute Of Science and Technology (FISAT) <br/> <span>B.Tech</span> in Computer Science and Engineering</p>
                <h1 style={{textAlign:'left'}}>Hobbies</h1>
                   My Hobbies involve Painting, Travelling and Singing. 

              	</div>
		</div>
	</div>
	<div className="container-fluid home-content2">
		<p>Sharing knowledge is the most fundamental act of friendship. Because it is a way you can give something without loosing something.</p><span>Richard Stallman.</span>
	</div>
    <div className="container-fluid home-content1">
		<div className="row" id="skills">
			
			<div className="col-md-6 content1-right our-skills">
                <h1>My Skillbar</h1><br/>
            <div className="skills-progress">
                            <div className="progress">
                                <div className="progress-bar progress-bar-success" role="progressbar"  style={{width:'90%'}}>
                                    HTML/CSS
                                </div>
                            </div>
                            <br/>
                            <div className="progress">
                                <div className="progress-bar progress-bar-danger" role="progressbar" style={{width:'50%'}}>
                                    React
                                </div>
                            </div>
                            <br/>
                            <div className="progress">
                                <div className="progress-bar progress-bar-warning" role="progressbar" style={{width:'70%'}}>
                                    Javascript 
                                </div>
                            </div>
                            <br/>
                            <div className="progress">
                                <div className="progress-bar progress-bar-info" role="progressbar" style={{width:'80%'}}>
                                    Bootstrap
                                </div>
                            </div>
                        </div>
            </div>
            <div className="col-md-6 content1-left">
				<h3>My Daily Inpiration <span className="blinker">!</span></h3>
                <img src="https://upload.wikimedia.org/wikipedia/commons/e/ed/Elon_Musk_Royal_Society.jpg" className="img-thumbnail" alt="..." width="50%"/>
				<p>Elon Musk.</p>
			</div>
		</div>
	</div>
	<div className="container-fluid content1-right contact-bg" id="contact">
				<h3>Contact Me <span className="blinker">!</span></h3>
				<p>Hi I am Neetha Francis.</p>
    <div className="text-center center-block">
                <a href="https://www.facebook.com/bootsnipp"><i id="social-fb" className="fa fa-facebook-square fa-3x social"></i></a>
	            <a href="https://twitter.com/bootsnipp"><i id="social-tw" className="fa fa-twitter-square fa-3x social"></i></a>
	            <a href="https://plus.google.com/+Bootsnipp-page"><i id="social-gp" className="fa fa-google-plus-square fa-3x social"></i></a>
	            <a href="mailto:bootsnipp@gmail.com"><i id="social-em" className="fa fa-envelope-square fa-3x social"></i></a>
</div>  
        	</div></div>
    )
  }
}
